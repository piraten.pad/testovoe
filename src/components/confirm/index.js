import Vue from 'vue'
import ConfirmVue from './Confirm.vue'

const ConfirmConstructor = Vue.extend(ConfirmVue)

const Modal = (options = { text: 'Подтвердите действие', okClick: () => {} }) => {
  const instance = new ConfirmConstructor({
    propsData: options,
  })

  instance.$mount()
  document.getElementById('app').appendChild(instance.$el)

  return instance
}
export default Modal
