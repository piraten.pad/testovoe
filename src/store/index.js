import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    notes: [
      {
        id: 0,
        text: 'Ваша первая заметка',
        checkboxes: [
          {
            id: 0,
            done: false,
            text: 'Кликни на меня',
          },
        ],
      },
    ],
  },
  mutations: {
    ADD_NOTE: (state, payload) => {
      if (!payload.text || !payload.text.trim()) {
        return
      }
      payload.id = state.notes[0] ? state.notes[0].id + 1 : 0
      payload.text = payload.text.trim()
      state.notes.unshift(payload)
    },
    DELETE_NOTE: (state, payload) => {
      const noteId = state.notes.findIndex((note) => note.id === payload)
      if (noteId !== -1) {
        state.notes.shift(noteId)
      }
    },
    EDIT_NOTE: (state, payload) => {
      const noteId = state.notes.findIndex((note) => note.id === payload.id)
      if (noteId !== -1) {
        state.notes[noteId] = payload
      }
    },
  },
  actions: {
    CREATE_NOTE: (ctx, payload) => {
      ctx.commit('ADD_NOTE', payload)
    },
    UPDATE_NOTE: (ctx, payload) => {
      ctx.commit('EDIT_NOTE', payload)
    },
    REMOVE_NOTE: (ctx, payload) => ctx.commit('DELETE_NOTE', payload),
  },
  getters: {
    NOTES: (state) => state.notes,
    GET_NOTE: (state) => (id) => state.notes.find((note) => note.id === id),
  },
  plugins: [createPersistedState()],
})
