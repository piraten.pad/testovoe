import Vue from 'vue'
import VueRouter from 'vue-router'
import NotesList from '../views/NotesList.vue'
import Note from '../views/Note.vue'
import NewNote from '../views/NewNote.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: NotesList
  },
  {
    path: '/new',
    name: 'New',
    component: NewNote
  },
  {
    path: '/note/:id',
    name: 'Note',
    component: Note
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
